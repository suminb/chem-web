
var coverImages = new Array("cover1.jpg", "cover2.png", "cover3.jpg");
var currentPage = 0;
var numberOfPages = coverImages.length;

function nextPage() {
    currentPage = (currentPage + 1) % numberOfPages;
    goToPage(currentPage);
}

function goToPage(page) {
    currentPage = page;
    //$("#cover").attr("src", coverImages[currentPage]);
    
    // Update page indicator
    $(".dot").removeClass("current");
    $("#dot" + page).addClass("current");
    
    // Transition
    $("#cover" + ((page-1+numberOfPages) % numberOfPages)).fadeOut("fast", function() {
        // Upon completion of the transition
        $("#cover" + (page % numberOfPages)).fadeIn("fast");
    });
}

window.onload = function() {
    $(".cover").click(nextPage);
//     for (var i = 0; i < numberOfPages; i++) {
//         $("#dot" + i).click(function() { goToPage(i); });
//     }
}